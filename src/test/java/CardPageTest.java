import AbsPageComponents.CardsComponent;
import AbsPageComponents.MainHeaderComponent;
import driver.DriverFactory;
import exceptions.DriverNotSupportedException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pages.MainPage;

public class CardPageTest {
    private WebDriver driver;

    @BeforeMethod
    public void init() throws DriverNotSupportedException {
        this.driver = new DriverFactory().getDriver();
    }

    @Test
    public void checkCardHeader() {
        new MainPage(driver)
                .open("");
        new MainHeaderComponent(driver)
                .chooseCard();
        String pageTitle = driver.getTitle();
        Assert.assertEquals(pageTitle, "Online Kart Sifarişi - Debet və Kredit kart- ABB Bank Kartları");
        new CardsComponent(driver)
                .chooseCard()
                .pageHeaderShouldBeSameAs("TamKart MasterCard Debet");
    }

    @AfterMethod
    public void close() {
        if (this.driver != null) {
            this.driver.close();
            this.driver.quit();
        }
    }
}
